<?php

/** 
 * @file
 *
 * Views handler for the comment_publish link.
 *
 * @author: Elliott Foster
 * @copyright: NewMBC 2010
 */

class views_handler_field_comment_publish_link extends views_handler_field {
  function construct() {
    parent::construct();

    // Add the fields that the comment_publish link will need..
    $this->additional_fields['nid'] = array(
      'table' => 'comments',
      'field' => 'nid',
    );
    $this->additional_fields['cid'] = array(
      'table' => 'comments',
      'field' => 'cid',
    );
  }

  function query() {
    $this->ensure_my_table();
    $this->add_additional_fields();
  }

  function render($values) {
    $node = node_load($values->{$this->aliases['nid']});

    if (comment_publish_access($node, $values->{$this->aliases['cid']})) {
      $comment = db_fetch_object(db_query("SELECT * FROM {comments} WHERE cid=%d", $values->{$this->aliases['cid']}));
      return l(
        t('@action', array('@action' => ($comment->status ? 'Publish' : 'Unpublish'))),
        'node/' . $node->nid . '/comment_publish/' . $comment->cid,
        array('query' => drupal_get_destination())
      );
    }

    return '';
  }
}

