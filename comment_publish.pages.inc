<?php

/**
 * @file
 *
 * Page callbacks for the comment_publish module.
 *
 * @author: Elliott Foster
 * @copyright: NewMBC 2010
 */

/**
 * Returns a form to (un)publish a comment.
 */
function comment_publish_publish_form($form_state, $node, $cid) {
  $form = array();
  $comment = db_fetch_object(db_query("SELECT * FROM {comments} WHERE cid=%d", $cid));

  $form['nid'] = array('#type' => 'value', '#value' => $node->nid);
  $form['cid'] = array('#type' => 'value', '#value' => $comment->cid);
  $form['action'] = array('#type' => 'value');

  if ($comment->status) {
    $action = 'publish';
    $form['action']['#value'] = 0;
  }
  else {
    $action = 'unpublish';
    $form['action']['#value'] = 1;
  }

  $form = confirm_form(
    $form,
    t('Are you sure you want to @action this comment?', array('@action' => $action)),
    array(
      'path' => 'node/' . $node->nid,
      'fragment' => 'comment-' . $comment->cid
    ),
    t(
      'Submitting this form will @action this comment, making it @visibility to users on the site.',
      array('@action' => $action, '@visibility' => ($comment->status ? 'available' : 'unavailable'))
    ),
    t('@action', array('@action' => ucwords($action)))
  );

  return $form;
}

function comment_publish_publish_form_submit($form, &$form_state) {
  if (db_query("UPDATE {comments} SET status=%d WHERE cid=%d", $form_state['values']['action'], $form_state['values']['cid'])) {
    drupal_set_message(t('The comment was @action successfully', array('@action' => ($form_state['values']['action'] ? 'unpublished' : 'published'))));
  }
  else {
    drupal_set_message(
      t('There was a problem @action the comment. Please try again later.', array('@action' => ($form_state['values']['action'] ? 'unpublishing' : 'publishing')))
    );
  }

  if (!isset($_GET['destination'])) {
    $form_state['redirect'] = 'node/' . $form_state['values']['nid'];
  }
}

