<?php

/**
 * @file
 * 
 * This module allows comments to be published/unpublished by privileged users.
 *
 * @author: Elliott Foster
 * @copyright: NewMBC 2010
 */

/**
 * Implements hook_perm().
 */
function comment_publish_perm() {
  $perms = array();
  $types = node_get_types();
  foreach ($types as $type) {
    $perms[] = 'publish comments on own ' . $type->type . ' nodes';
    $perms[] = 'unpublish comments on own ' . $type->type . ' nodes';
  }

  return $perms;
}

/**
 * Implements hook_menu().
 */
function comment_publish_menu() {
  $items = array();

  $items['node/%node/comment_publish/%'] = array(
    'title callback' => 'comment_publish_title',
    'title arguments' => array(1, 3),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('comment_publish_publish_form', 1, 3),
    'access callback' => 'comment_publish_access',
    'access arguments' => array(1, 3),
    'file' => 'comment_publish.pages.inc',
  );

  return $items;
}

/**
 * Implements hook_views_api().
 */
function comment_publish_views_api() {
  return array('api' => 2.0);
}

/**
 * Implements of hook_views_handlers().
 */
function comment_publish_views_handlers() {
  $handlers = array();

  $handlers['handlers'] = array(
    'views_handler_field_comment_publish_link' => array(
      'parent' => 'views_handler_field',
      'file' => 'views_handler_field_comment_publish_link.inc'
    ),
  );

  return $handlers;
}

/**
 * Implements hook_link().
 */
function comment_publish_link($type, $object, $teaser = FALSE) {
  $links = array();
  if ($type != 'comment') {
    return $links;
  }

  global $user;
  $node = node_load($object->nid);

  switch ($object->status) {
    case 0: // published
      if (!user_access('unpublish comments on own ' . $node->type . ' nodes')) {
        return $links;
      }
      $links['comment_publish'] = array(
        'title' => t('unpublish'),
        'href' => 'node/' . $node->nid . '/comment_publish/' . $object->cid,
        'query' => drupal_get_destination(),
      );
      break;
    case 1: // unpublished
      if (!user_access('publish comments on own ' . $node->type . ' nodes')) {
        return $links;
      }
      $links['comment_publish'] = array(
        'title' => t('publish'),
        'href' => 'node/' . $node->nid . '/comment_publish/' . $object->cid,
        'query' => drupal_get_destination(),
      );
      break;
  }

  return $links;
}

/**
 * Returns a title string for a comment_publish form.
 */
function comment_publish_title($node, $cid) {
  $status = db_result(db_query("SELECT status FROM {comments} WHERE cid=%d", $cid));

  if ($status) { // unpublished
    $title = 'Publish this comment';
  }
  else { // unpublished
    $title = 'Unpublish this comment';
  }

  return t($title);
}

/**
 * Access callback for comment_publish forms.
 */
function comment_publish_access($node, $cid) {
  if (!$node || !$cid || !is_numeric($cid)) {
    return FALSE;
  }

  global $user;

  if ($user->uid != 1 && $user->uid != $node->uid) {
    return FALSE;
  }

  $comment = db_fetch_object(db_query("SELECT * FROM {comments} WHERE cid=%d", $cid));

  // make sure the node and comment match
  if ($node->nid != $comment->nid) {
    return FALSE;
  }

  if ($comment->status) { // unpublished
    return user_access('publish comments on own ' . $node->type . ' nodes');
  }
  else { // published
    return user_access('unpublish comments on own ' . $node->type . ' nodes');
  }
}

